from django.urls import path
from . import views

app_name = "apps_Story_4"

urlpatterns =  [
    path('', views.Home, name='home'),
    path('profile', views.Profile, name='profile'),
    path('experiences', views.Experiences, name='experiences')
]
